package de.fhbingen.epro.vl3.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;

import de.fhbingen.epro.vl3.ioc.basic.DefaultMessage;

public class ProjectStartJavaConfig {

	public static void main(String[] args) {
		ApplicationContext ctx = 
			      new AnnotationConfigApplicationContext(BaseConfiguration.class);
		
		Assert.notNull(ctx);
		
		DefaultMessage defaultMessage = (DefaultMessage) ctx.getBean("defaultMessage");
		
		Assert.notNull(defaultMessage);

	}

}
