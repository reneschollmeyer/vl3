package de.fhbingen.epro.vl3.di;

import de.fhbingen.epro.vl3.model.Candidate;

public interface VoteRecorder {

    public void record(Candidate candidate) ;

}
